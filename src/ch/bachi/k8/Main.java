package ch.bachi.k8;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.halt;

public class Main {

    private static boolean killedInstance = false;

    public static void main(String[] args) {

        System.out.println("Starting server...");

        before((request, response) -> {
            if (killedInstance) {
                halt(500, "This instance ain't no more..");
            }
        });

        get("/hello", (req, res) -> "Hello World");

        get("/calc/:number", (req, res) -> {

            double pi = 0;
            int number = Integer.parseInt(req.params(":number"));

            for (int i = number; i > 0; i--) {
                pi += Math.pow(-1, i + 1) / (2 * i - 1); // Calculate series in parenthesis
                if (i == 1) { // When at last number in series, multiply by 4
                    pi *= 4;
                    return "Result: " + pi;
                }
            }
            return "dooh...";
        });

        get("/kill", (request, response) -> {
          killedInstance = true;
          return "current instance killed... ;-)";
        });

    }
}
